sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

my %h; 
my @columns;
while(<STDIN>) { 
	my $line = $_; 
	my @a=split(/\|/, $line);
	my $index=0;
	foreach $c (@a) { 
		push(@{$columns[$index]}, trim($c));
		$index++;
	}
}

my @lines;
foreach $c (@columns) {
	my @trimmed = grep(defined && length, @{$c});
	my $str=join(' OR ', @trimmed);
	$str="( $str )";
	push(@lines, $str);
}

my $output;
$output = join(' AND ', @lines);

print "$output\n";
