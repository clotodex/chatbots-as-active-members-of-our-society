#!/bin/bash
tail -n+3 keywords.md | tr -d '\\\\' | awk '{$1=$1};1' | sed -re "s/^\|*//g" | sed -re "s/\|$//g" | perl test.pl
