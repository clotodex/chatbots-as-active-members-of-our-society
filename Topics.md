# Main Topics

- Manipulation of the average person
	- Chatbots
	- Social Networks
	- Advertisements
	- Elections
- Semantic Analysis / (Re)Creation of private data from public information
	- when someone is on holidays based on their facebook status
//- Reading of communal data
//	- prediction of election results
//	- trending topics in economy
//- Impact on our daily life (more or less a list)
//	- Social media
//	- Internet
//	- Search Engines
//	- in general: Tools that use datamining in its background

# Subtopics

- datamining as part of our society/social media (chatbots, recommendation systems, statistics and analysis)
- //An overview over datamining in social media

- the impact of chatbots on the end user of social media
- chatbots as active members of our society
- Manipulation of public Opinion with DataMining in Social Media = datamining as a tool for influencing social media users
- impacts of datamining in social networks on the end user (smart advertisement, chatbots, recommendation systems,

- an overview over opinion mining
- //Social media as a research source with Data Mining

# Chapters

- intro: How Bots Will Completely Kill Websites and Mobiles Apps
- definition of a chatbot
- history of chatbots - why do they exists
- different types of chatbots (rule based, machine learning based)
- use cases
- malicious chatbots
- Löbnner Prize
- development of a chatbot (different strategies)
- the datamining part in chatbots
- detection
- a simple chatbot with tensorflow

# Keywords

opinion mining
social networks
malicious bot
bot social media
politics
sentiment analysis

# Authors

Liu Huan
