| what          | how         | where            | general       | data |
|---------------|-------------|------------------|------------   |------|
| opinion       | predict     |                  | mining        | data |
| decision      | manipulat\* |                  | psychology    |      |
| election      | influence   | social media     | deep learning |      |
| deci\*        | detect\*    |  chat bot\*      |               |      |
| sentiment     | recreat\*   | social network\* |               |      |
| ad\*          | affect\*    | politic\*        |               |      |
| advertisement | bias\*      |                  |               |      |
|               | exploit\*   |                  |               |      |
|               | malicious   |                  |               |      |
