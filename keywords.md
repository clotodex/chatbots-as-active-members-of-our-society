| what       | how         | where            | general       | data |
|------------|-------------|------------------|------------   |------|
| opinion    | predict     | ad\*             | mining        | data |
| decision   | manipulat\* | advertisement\*  | psychology    |      |
| election   | influence   | social media     | deep learning |      |
| deci\*     | detect\*    |  chat\* bot\*    |               |      |
| sentiment  | recreat\*   | social network\* |               |      |
|            | affect\*    | politic\*        |               |      |
|            | bias\*      |                  |               |      |
|            | exploit\*   |                  |               |      |
|            | malicious   |                  |               |      |
